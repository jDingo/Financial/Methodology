#### Disclaimer: I do not claim this information is accurate, nor that it will be the best information for your situation. More importantly, there is no guarantee that you will be better off with this method than if you followed any other advice. This is constructed from personal research and experience. You may do whatever you wish with **your** money


# Phase I 
This is a setup phase for basic financial independence. You will set up automatic 401k deposits, create a budget, get a credit card, and save money. This will lay the groundwork for Phase II 

### Invest up to company match into 401k (preferably Roth 401k if available) 
* If company matches up to 6% of your paycheck, invest 6% into 401k 
* This is free money. Take it. Instant 100% return on investment into your future 
    * Like seriously, this is the single MOST important thing to do before anything else 
* For now, opt for a Target Date fund 
    * Target Date Funds automatically rebalance composition as you get closer to retirement year, such as 2055 or 2060. (Learn more about portfolio composition and balancing later in Phase II’s “Open an IRA”) 

### Contribute to Health Savings Account (HSA) for company match if offered
* Usually only offered for high-deductible health insurance plans
* Similar to 401k contributions and matching, if your company contributes and/or matches HSA contributions, take advantage because FREE MONEY
* I'll cover more about this is Phase II's "Contribute to HSA"

### PAY OFF ENTIRE CREDIT CARD BALANCE EVERY MONTH! 
* I will cover this more later under credit card section, but just want to emphasize importance 

### Avoid buying a car until you can pay for the entire car all at once, or unless you absolutely need a replacement 
* Similar to point above, a pretty important point that needs to be addressed while I still have your attention 
* Cars are expensive and monthly car payments make them even more expensive 
* You’ll save way more money by buying a car with one payment because you will avoid interest costs, and the money you save up will earn interest, thus in a sense making your car even cheaper 
* Less hassle when buying a car because you don't have to worry about loans and all that headache
* Added bonus, showing up to a dealership with the case to buy a car outright is a great negotiating tactic

### Set up direct paycheck deposit into savings account
* I mean what's not to love? It's instant and you don't have to worry about bank hours or it getting lost in the mail

### Create an account in your financial tool of choice and connect checking, savings, credit cards, 401k (if able), credit cards, Roth/Traditional IRAs, investment accounts, and any loans you may have 
* Mint is a pretty solid all-in-one financial overview tool, so feel free to explore it in depth 
* A good alternative to Mint is Personal Capital, but it’s focused more on investments 
* An even better option to stay on top of your spending would be Quicken or YouNeedABudget where you can add transactions manually so you are always aware of where your money is going. These are paid options however. Alternatively you could build your own Excel spreadsheet or app. (Speaking of... I may eventually get around to building one)

### If you have student loan or any other pre-existing debt like credit cards (excluding mortgages), see Appendix I and disregard budget percentages in next bullet (but still read it!) 

### Make a budget in Mint (for take home pay; ignore 401k and things deducted from paycheck) 
* Ideal budget goal (though acceptable if any of these change +/- 5%): 
    * <35% for housing necessities (rent, electricity, gas, water, trash/sewage, etc.) 
    * \>30% into savings (savings account, Roth/Traditional IRA, stock portfolio)
    * <35% for purchases (shopping, food, internet, cell phone, etc.) 
* Remember that is an IDEAL situation. Don’t starve yourself to save 35% of your paycheck. And don’t pass up the occasional expensive event with friends (within reason) 
* Stick to your budget, Harry. Stick. To. Your. Budget. This will be especially important in next bullet when you get a credit card 
* It may take a couple months to really nail down how much to budget in each category; feel free to adjust it as needed (but in a reasonable way. No $5,000 for eating out each month)
* Tip: Eating at home is much less expensive and often healthier than eating out. 
    * $5-6 for one sandwich at Subway vs $5-6 for ingredients to make a bunch of sandwiches at home! 

### Get a rewards credit card (Though if getting your first credit card, may have to settle for non-rewards card for the time being since you probably do not have a credit score required for rewards cards) 
* When applying for card, add any bonuses and such to your salary when prompted. Increases chances of approval as well as credit limit, which helps credit score, which helps get better rates on mortgages, or God forbid an auto loan 
* Citi Double Cash (requires somewhat high credit score) earns 1% cash back with your purchase, and then additional 1% when you pay off that purchase (comes out to 2% cash back!)
* Discover It (more likely to get approved than Citi Double Cash) earns 1.5% cash back 
* Chase Freedom (unsure for approval) 5% cashback in rotating categories (5% on groceries during certain months, then 5% cashback at home improvement stores other months, etc) 
* American Express Delta SkyMiles or Citi American Airlines travel reward cards (if you fly maybe 2+ times a year) 
* [NerdWallet.com](https://www.nerdwallet.com) does a great job breaking down different credit cards to fit your needs 

### Make all purchases on credit card (unless you don’t feel comfortable doing this, in which case only use it to buy gas or something simple to build credit). Some apartment complexes offer credit card payment options, but usually charge a fee to do so. Avoid this 
* More secure  
    * If someone steals your card, credit card companies are super great at reimbursing contested payments 
    * Your personal money is not at risk if account hacked/card stolen 
        * This literally just happened to a girl I work with where her debit card was hacked and money was taken directly from her checking account. She literally didn’t have any money to do anything until bank reimbursed her 
* Cash back is awesome (or whatever reward you get with card) 
* Savings account earns interest all month until it’s used to pay off card 
* Don’t have to worry about overdrawing checking account 
    * Turn off overdraft protection for savings account  
    * 22% of $10 on a credit card (if you don’t pay it off immediate, WHICH YOU SHOULD IF YOU CAN!) is better than $30 overdraft charge for not having enough for a $10 charge 
* Builds credit score (important for renting apartments, financing a car [which you should avoid if possible] buying a house, etc.) 
    * There are also several benefits to a good credit score, beyond just reduced interest on loans. Some apartment complexes will waive application fees, security deposits, etc. if your credit score is above a certain threshold. Additionally, higher credit score will open up opportunities for better reward credit cards that provide higher cash-back percentages, etc. 

### PAY OFF ENTIRE CREDIT CARD BALANCE EVERY MONTH! 
* As in the total balance on most recent statement. So, if you have $1,000 bill for July statement, and have since made purchases since the end of that statement period for $100, only pay the $1,000 in your July bill (unless you just want to pay off all $1,100. No advantage or disadvantage either way) 
* If you are late on a bill, you will incur an interest charge (probably ~20%). Avoid paying at all costs.  
    * If you leave a balance of $1,000 to pay at a later date, you will be charged $200 (20%) in interest for that amount. Now you can see why people can easily get into financial ruin if not careful with credit cards and pay for things they can’t currently afford. DON’T BE LATE! 
* Late payments also hurt credit score. Badly. So, don’t be late on a payment (though paying at least minimum balance for billing period exempts you of lateness…but then you deal with interest. See above bullet point 


### Set-up automatic bill-pay
* I previously paid all my bills on the same day each month, then shifted to every payday, but I've since converted to automatic bill-pay. 
* If you're concerned about companies having your account or credit card information, fear not! Most banks should allow you to set up "push" payments rather than having companies "pulling" from your account
* The convenience of not having to worry about it is just so nice
* I would recommend NOT setting up automatic credit card payments, however. It's best for you to keep a close eye on that one.
    * Each paycheck, I look ahead at upcoming expenses in my checking account and determine how much to pay towards my credit cards. Your credit card website should display how much is still owed for a statement balance
* Tip: Some bills (such as phone, internet, electricity) can be paid on credit card, for more cash back! (I know 2%, or $2, back on $100 purchase may not seem like much, but it really adds up over time!)

### Stick to your Mint budget 
* Treat remaining amount in your budget as if it’s your checking account. This will prevent you from spending more than you can afford on your credit card, thus preventing interest fees.  
* The more under budget you go, the more you throw into savings, the more that money earns in interest from the bank, the higher your net worth gets 

### Associate with money-conscious people 
* If your core group of friends are constantly blowing money, you will be more likely to also blow all your money, Similarly, if your core group of friends are money-conscious, you won’t likely be going out to fancy restaurants with them every night 
* Doesn’t mean you should ditch your friends if they aren’t money conscious, just try to get them on the same page as you and you’ll all benefit.  
* Knowing your friend is also saving money, you will be inclined to compete with him/her. Even if they don’t know you’re competing, let it get under your skin if you perceive they have saved more money than you. 
* Being open and talking about your finances with your friends (even if not in detail) will help fill any knowledge gaps, as well as keep your finances on your mind. Plus, you may guilt yourself out of buying something expensive and unnecessary 


# Phase II 
So, you have most things established. Now for the fun stuff! You’ll create an emergency fund, set up a Roth 401k, and maximize 401k contributions. This is where you’ll start building a foundation for your wealth 

### Create an emergency fund 
* An emergency fund is meant to be used in case of emergency, such as car breaking down and needing to pay for expensive repairs, losing your job and needing to still pay bills until new job comes along, a pandemic, etc. 
* Typical emergency funds are 3-6 months of expenses. (though I personally prefer 3-6 months of **income**, just to be safe) 
    * If you have a budget of $1,500 for housing necessities, $1,500 for general purchases, and $1,500 going into savings, you want your emergency fund to cover 3-6 months of $3,000 in expenses, or $9,000-$18,000 (if an emergency arises and you are not earning income while withdrawing from your emergency fund, you won’t be saving $1,500/month, which is why you’re saving $3,000/month and not $4,500/month) 
* You want this money to easily accessible and safe from economic turmoil 
    * Put in a high interest savings account (following rates may have changed since writing this) 
        * Ally Bank – 1.05% savings accounts 
        * Discover Bank – 1.10% savings accounts 
        * Most “brick & mortar” banks have rates close to about 0.10-0.15% 
    * DO NOT PUT IN AN INVESTMENT ACCOUNT DESPITE THE TEMPTATION TO INVEST SO MUCH MONEY 
        * This could result in losing some of your fund if you need to withdraw during a bear market 
        * Less accessible due to multi-day transfer time 
        * Emergencies and market fluctuations are not usually timed conveniently (lay-off from work due to down economy). So, drawing from a down market investment account during an emergency will not bode well 
* Keep this in a separate savings account from short-term savings and checking 
    * I have 3 banks accounts (all with Ally Bank at the moment) 
        * Emergency Savings Account 
            * Earns 1.05% annual interest 
        * Expense Savings Account 
            * Earns 1.05% annual interest 
            * This is where my paychecks get deposited and earn interest until bills are due 
            * When bill day comes (1st of the month for me), I move every cent to my checking account 
        * Checking Account  
            * Earns 0.25% interest, but typical checking accounts do not earn interest 
            * This account stays empty until bill day. Once all bills are paid, remainder moves to Emergency Savings Account 
* Get your emergency fund up to your desired 3-6 months’ worth of expenses before starting next step (or, once you’ve hit 3 months of expenses, slow down on Emergency Fund contributions while you start next step, and then stop contributing once you’ve hit 6 months) 

### Maximize HSA Contributions
* Health Savings Accounts (HSA) are similar to an IRA or 401k, but specifically for medical costs
* HSA contributions, growth, and withdrawls for **eligible** medical expenses are all tax-free
* The nice thing about HSAs is that you can cover **eligible** medical expenses with it at anytime without penalty and not wait until retirement age
    * But once you have hit retirement age, you can withdraw from your HSA for non-medical expenses without penalty, but such distributions will be taxed as regular income similar to a traditional 401k distribution
    * This means there's no such thing as "over-funding" an HSA, since you don't have to use it on medical costs in retirement, though you should avoid that if possible for the tax benefits because you'll ideally have plenty in IRA and 401k to cover non-medical expenses in retirement
* Similar to IRA and 401k, there is an annual contribution limit (currently $3,600 for individual coverage and $7,200 for family coverage) that often changes each year, so you'll weant to research beforehand

### Open a Roth IRA 
* An IRA is an Individual Retirement Account 
* You are allowed to invest up to $6,000 into an IRA each year (usually increases each year, so research limit for current year)
* At 55 the contribution limit goes up to 6,500, as of the time of this writing
* You are not allowed to withdraw from this fund without penalty until you are 59 ½ years old 
* Roth IRA means the money being invested has already been taxed as income, and therefore will not be taxed again when you withdraw in retirement 
* Traditional IRA means you invest money that has not yet been taxed, but will be taxed upon withdrawal in retirement 
    * When first starting your career, it’s likely that you will be in a lower tax bracket than you will be in retirement. Thus, it’s better to pay 25% income tax now than 28% tax at withdraw (research tax brackets for your current year, as they may change as well) 
    * This same concept applies to Roth 401k, so if your employer offers Roth 401k option, be sure to invest in that for early career 
* Several brokerage companies offer Roth and Traditional IRAs 
    * E-Trade 
    * Schwab 
        * $4.95 trade commissions 
        * Multiple Commission-Free ETFs and Mutual Funds 
    * TD-Ameritrade 
        * Multiple Commission-Free ETFs 
    * “Robo-Investors” 
        * Annual percentage fee 
        * Handles all investments for you based on your risk tolerance 
        * These are especially hands-off and convenient, but by doing a little bit more research on investing in Index Funds, you can get similar or better performance by investing yourself, and you will avoid paying more in fees as roboinvestors usually charge about a 0.20%-0.30%, while building your own portfolio of ETFs or mutual funds can keep you down to about 0.10%
* Investment accounts can have money direct deposited from your paycheck, so set this up for automatic contributions with each paycheck. Strive to reach contribution limit for each year as quickly as possible 
* Once money is in an IRA, you still need to buy shares, bonds, mutual funds, ETFs 
    * There is A LOT of information on the internet on how to invest. Basics: 
        * Sell high; buy low (though for long term investments like an IRA, it’s not as big of a deal) 
        * Stocks earn higher interest rates, but are riskier 
        * Bonds earn lower interest rates, but are safer 
        * Stock and Bond prices typically move in opposite directions 
        * Market crashes, while unfortunate, make for the best time to buy if you can afford it. But ideally for long term investing, just buy as soon as you have investable cash in your portfolio 
        * Mutual fund is a grouping of stocks and/or bonds managed by a “Fund Manager”. Each mutual fund will have different stock/bond ratio, so pick one that fits your desired Risk level 
        * ETF (Exchange Traded Fund) is like a mutual fund, but can be traded like a stock or bond 
        * Some stocks, bonds, mutual funds, and ETFs pay Dividends, which means they pay you as a “thanks” for holding their stock
            * You can either collect this as cash into your portfolio, or have that Dividend reinvested back into the stock or fund, where you will be rewarded with partial shares of the company
            * Dividend Re-Investment Plan, or DRIP. Like drips into a bucket, which after 40+ years of dripping becomes a pond or even a lake with a bucket in the bottom. 
                * For Retirement or Long-Term investment accounts, opt into the DRIP. For shorter investment timelines, you can not opt in, but instead keep the dividends safe as Cash in your portfolio 
    * In your 20s, you want a stock/bond ratio of at least 70% stocks/30% bonds 
        * This will be riskier, but earn higher returns 
        * If market crashes, your investments will suffer but you have decades to recover 
    * Each decade of age, rebalance -10% stocks / +10% bonds (or just shift by 1% each year) 
        * 30s: 60% stocks / 40% bonds 
        * 40s: 50% stocks / 50% bonds 
        * 50s: 40% stocks / 60% bonds 
        * 60s: 30% stocks / 70% bonds 
        * Retirement (65): 20% stocks / 80% bonds 
    * Avoid high commissions, and avoid high fees 
        * Commissions and fees will chip away at your earnings 
        * Let’s say your IRA is worth about $500,000, and your average fees are about 1%. That’s $5,000/year just to pay the salary of the Fund Manager, and usually Fund Managers fail to “beat the index” (see below), or might even just be an automated system
        * Index funds are usually set to simply track performance of an Index, like S&P500, NASDAQ, DOWJONES, etc, and have incredibly low fees close to 0.05%, which is only $250 of your $500,000 account! 
    * I personally (currently 25 years old) have a 90% stock / 10% bond ratio invested across a few Schwab Commission-Free ETFs that have an average of < 0.10% in fees (though each investment company may have their own with similar specs) 
        * SCHB – Follows DOW Jones Index; Moderately High Return; Low Fee 
        * SCHA – Aggressive Fund; High Returns (high risk); Moderately Low Fee 
        * SCHD – Dividend Fund; Moderate Returns (medium risk); High Dividends; Low Fee 
        * SCHF – Foreign Established Markets; Moderate Returns; Moderate Fee 
        * SCHE – Foreign Emerging Markets; High Returns (risky); Moderate Fee 
        * SCHX – Bond Fund; Low Returns; Low Fee 
    * See Appendix II for more details 
* Any online investment broker will have an entire section for learning about investing. Take advantage of this, as well as multiple websites (www.investopedia.com is a great resource) 
* Many brokers, such as TDAmeritrade, Schwab, E-Trade, Merrill-Lynch, etc., will provide a free consultation meeting when first setting up your account. You can either call, or go to a local branch if available to take advantage of this consultation and get your account set-up. After the first free consultation, you will likely pay a fee in some way, so be sure to ask or research about it 
* Assuming you’ve already got 3-6 months’ worth of expenses built up in your Emergency Fund, open an IRA and throw any money you were previously throwing into your Emergency fund into the IRA each month, until you hit your $6,000 annual IRA contribution limit. Then, move on to the next step in this phase 

### Maximize your 401k contributions 
* A 401k has an annual contribution limit of up to $19,000 (often changes each year, so research the current limit) 
* You will incur serious penalty if you withdraw from a 401k before you are 59 ½ years old 
* Increase you 401k contributions each year until you reach the $19,000 contribution limit 
    * Only after you have fully funded your Emergency savings account with 3-6 months’ expenses (if you currently have 3 months’ expenses, consider moving towards 6 months) 
    * And only after you have reached your $6,000 annual IRA contribution limit for the year (if you don’t think you can hit your $6,000 limit for a year, feel free to dial back 401k contributions, **BUT MAKE SURE YOU ARE CONTRIBUTING AT LEAST THE COMPANY MATCH; FREE MONEY!**
    * But first reward yourself a little bit by increasing your general purchases budget a little bit. You deserve a little money for your present self. Retirement you shouldn’t have all the fun 
* It may take a few years’ worth of raises and bonuses before you are maximizing your 401k contributions, so don’t feel like you’re behind if you’re not contributing $19,000/year by age 30 
* If you want to be more active in your 401k portfolio, now would be the time to move away from a Target Date Fund, but there’s nothing wrong with sticking with the Target Date Funds. They’re convenient, automatically rebalance, and will probably earn higher returns than anything you try to do yourself
* You may be wondering why maximizing 401k comes after maximizing IRA 
    * It’s psychologically easier to tackle smaller chunks 
    * Will be able to maximize $6,000 faster than $19,000 
    * Likely to pay lower fees with IRA funds than 401k funds, unless employer offers excellent fund options 
    * More control over your investments (though not a crucial reason) 
    * 401k may not offer Roth contribution options, which can be big deal if you believe you will move to a higher tax bracket in retirement 
    * You are allowed to withdraw principal (money you contributed, so not the gains your money made) from an IRA without penalty as long as you pay it back (be sure to research extensively before you ever do withdraw from IRA to avoid any penalties), which can be useful if you exhaust your Emergency Fund for some reason (but should still try to avoid withdrawing from IRA if you can) 
* It’s acceptable to reduce 401k contributions from time to time, such as saving for a house, re-funding your Emergency Fund if you had an emergency, saving for a car, saving for college for children, or any other number of reasonable reasons you may think of that requires reducing 401k contributions 
* Once you are contributing $6,000 to your IRA and $19,000 to your 401k, and have a fully funded Emergency Fund, move on to the next phase 


# Phase III 
By now, your 401k contributions should have wiped out your Savings budget category, your Emergency fund should be nice and full and earning a low but steady interest rate (drips in a bucket), and yet you still have more money coming in than you can effectively allocate. First of all, congratulations. You are now saving $25,000 ($19,000 + $6,000 for 401k & IRA, respectively) every year for retirement. You likely have a net worth of at least $100,000 by now, possibly even $200,000. That’s insane. Have you ever in your life thought that you would have more money in your name than the value of multiple luxury cars? That is an incredible feat. But you know what’s even more incredible? You now have excess income that you need to do something with. And you know what’s even more incredible than that? Compound interest 

### Compound Interest 
* “Compounding interest is the most powerful force in the universe” -Supposedly Albert Einstein
* Each year, the interest you earn applies to your principal investment PLUS any interest it has already earned 
* With a $100 investment that, let’s say, earns an average of 10%  in interest annually (percentage returned is dependent on the strength of economy and stock market), you will have $110 dollars after one year (Math Check: 100 * 1.10 = 110. Yep, math checks out) 
* That $110 earns another 10% interest throughout the next year. Except this time, you will have $121 at the end of year 2 (Match Check: 110 * 1.10 = 121. Thank you, math) 
* Year 3, you will have $133.10...
* And so on...
* And so on 
* At the end of 40 years, your $100 investment will be worth about $4,114.48, and earning more than $400 in interest each year 
* 40 years of maxed $5,500 IRA contributions each year earning 10% interest would be worth $2,677,684 and earning $243,425 in interest, annually (this does not factor rebalancing your stock/bond ratio over time to minimize risk based on your age, which would likely also lower returns) 
* Let’s say you maximized 401k contributions by age 35. After 30 years later when you retire, your 401k would be worth $3,256,982 and earning $296,089 in interest, annually (this does not factor rebalancing your stock/bond ratio over time to minimize risk based on your age, which would likely also lower returns) 
* That. Is compound interest (This concept works pretty much same way with debt where you pay interest. Except the interest going to someone else rather than you. It’s bad. Avoid debt at all costs.) 
* Learn it. Love it. Marry it. Have babies with it. Let those babies have babies. Watch your family tree compound. Profit 
* See Appendix III for more details 

### Open a Standard Investment Account 
* Standard brokerage accounts do not have an annual contribution limit 
* You are able to withdraw from this account at any time 
* Gains you earn from SOLD shares are subject to 15% capital gains tax if you held that particular share (not that particular company the share belongs to) for more than one year, and your standard income tax rate (see your tax bracket [likely around 25%]) if you held the share for less than a year 
    * Try to hold shares for longer than a year 
    * There are ways to minimize Tax Loss. Research Tax Loss Harvesting 
* Any money that you don’t have allocated to your IRA, 401k, or budget categories should get deposited into this account 
* Similar to your IRA, you have to make stock/bond/mutual fund/ETF purchases to earn interest 

### Accumulate wealth
* Do whatever you want. You probably have more money than me now and probably shouldn’t take financial advice from someone with less money than you 
* Once your investments are returning more than you are spending, you're completely financially independent (so the more you make and the less you spend, the faster you get there). Your money is literally working for you, and you don't have to lift a finger 


 
#### Most of this guide was derived from various sources across the internet, but especially the [PersonalFinance subreddit guide to money](https://www.reddit.com/r/personalfinance/wiki/commontopics)
#### Life-stage specific guides can be found on right side of the page as colorful rectangles 
 

# Appendix I – Student Loan Budgeting (or pre-existing debt) 
* Budget your necessities (housing, etc) 
* Try to reduce general purchases (shopping, bars, alcohol, etc) 
* Put $1,000 into Emergency fund (See Phase II’s Create an Emergency Fund) 
    * Contribute what you can initially until you reach $1,000 
    * Once you’ve saved $1,000, continue contributing about $10-$50 a month if you can spare it 
* Pay as much money as you possibly can towards your debt. 
    * In instances of multiple lines of debt, such as multiple loans, credit cards, etc., prioritize payments based on interest rates. Always pay the minimum payment for all your debts (never miss a payment), and then throw whatever is left at your highest interest debt until it is paid off, then move to your next highest interest debt 
        * There is also the “snowball” method where you prioritize your smallest debt (ignoring interest) so that you can gain the psychological benefit of seeing a payment disappear. This can be beneficial, but will cost more money in interest over the long-term 
* Once you’ve paid off all debt, continue budgeting as suggested in Phase I 


# Appendix II – Investing 
Coming later, if needed 
In the meantime, find some “Intro to Investing” resources online. Investopedia.com is a good place to start 

### Note on rebalancing: 
* As the market changes, some funds will be worth more than they were, and some may be worth less. Your portfolio will slowly shift out of balance 
    * For simplicity let’s say your starting portfolio had an allocation like this: 
        * 25% Aggressive Domestic Stock 
        * 25% Moderate Domestic Dividend Stock 
        * 20% Foreign Stock 
        * 15% Domestic Bonds 
        * 10% Foreign Bonds 
        * 5% Cash 
    * After a few months, your portfolio allocation may now look something like this: 
        * 21% Aggressive Stock 
        * 24% Moderate Domestic Dividend Stock 
        * 26% Foreign Stock 
        * 17% Domestic Bonds 
        * 8% Foreign Bonds 
        * 4% Cash 
    * You want to “rebalance” your portfolio to maintain your original allocation (which you will change over time, based on age, as pointed out in Phase II’s Open a Roth IRA) 
    * To rebalance above portfolio, you want to sell off some of your gainers and buy more of your losers (Buy Low; Sell High). Eventually your losers will turn around, and your gainers may stumble 
        * Sell 6% worth of Foreign Stock 
        * Sell 2% worth of Domestic Bonds 
        * Buy 4% worth of Aggressive Stock 
        * Buy 1% worth of Moderate Domestic Dividend Stock 
        * Buy 2% Foreign Bonds 
        * Cash will automatically get rebalanced once above transactions are completed 
    * It’s usually good to rebalance consistently, whether it be every month, every three months, or every 6 months 
        * It may be difficult to rebalance monthly when your account balance is relatively low just because a 1% change in an allocation group may not be worth an entire share of stock, but higher account balances may see a 1% change being worth several shares. 


# Appendix III – Compound Interest 
Coming Soon 
In the meantime, best thing is to open up Excel, Google Sheets, etc., and mess around, make some graphs. Here’s a starting point that you can plug into Excel (the column title “Year goes in cell A1): 

|       | *A*       | *B*                   | *C*                   | *D*                   | *E*       | *F*                           | *G*   | 
| ----- | --------- | --------------------- | --------------------- | --------------------- | --------- | ----------------------------- | ----- |
| *1*   | **Year**  | **Year Start Value**  | **Interest Earned**   | **Year End Value**    |           |                               |       |
| *2*   | 1         | =G2                   | =B2*$G$3              | =B2+C2                |           | **Annual Contribution**       | 18000 |
| *3*   | 2         | =B2+C2+$G$2           | =B3*$G$3              | =B3+C3                |           | **Assumed Average Return**    | 10%   |
| *4*   | 3         | =B3+C3+$G$2           | =B4*$G$3              | =B4+C4                |           |                               |       |
| *5*   | 4         | =B4+C4+$G$2           | =B5*$G$3              | =B5+C5                |           |                               |       |
| *6*   | 5         | =B5+C5+$G$2           | =B6*$G$3              | =B6+C6
